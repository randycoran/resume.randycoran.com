---
layout: default
published: true
title: Randy Coran Resume
---

# Randy Coran

## Contact Information

- Email: myemail@example.com
- Phone: (123) 456-7890

## Links
<details>
- [LinkedIn](https://www.linkedin.com/in/randy-coran/)
- [Gitlab](https://gitlab.com/randycoran)
- [Github](https://github.com/HomeServerTech)
</details>

## Qualifications
<details>
- AAS in Computer Information Systems
- 8+ years in a DevOps Linux / Cloud Engineer role
- AWS, Linux, Security, and Microsoft Certifications

- 14+ years System Administration / Engineering / DevOps
- 14+ years Linux Admin (RedHat, CentOS, Ubuntu, Debian)
- 10+ years ansible automation experience
- 10+ years Scripting with Bash and Python
- 7+ years Hashicorp Terraform experience
- 8+ years Windows Server Administration
- 3.8 years working in a secure DoD environment
</details>

# Experience

### Job Title, Company Name (Year - Year)

- Description of job responsibilities.
- Achievements or accomplishments.

### Previous Job, Another Company (Year - Year)

- Description of job responsibilities.
- Achievements or accomplishments.

## Education

### Degree, University Name (Graduation Year)

- Relevant coursework: Course1, Course2
- Thesis/Project: Title of thesis/project

## Skills

- Skill 1
- Skill 2
- Skill 3

## Certifications

- Certification 1
- Certification 2
